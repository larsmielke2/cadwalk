# cadwalk

# Simple tool to create / morph geometries using mouseWheel
## shell call like
- python -m cadwalk --help
- python -m cadwalk info

## python import
- import cadwalk
- from cadwalk.code import arguments
- from cadwalk.code import animate --> animate.main(\*\*arguments.mk_args().\__dict__)

NOTE:
- requires python 3.8 or higher
- Setup.py currently not working. Use Pipfile in setup dir to create environment!
- Pipfile location: cadwalk.setup_cadwalk.
- You can install Pipenv in setup_cadwalk and then copy .venv to your working dir
- NOTE: You also need a Pipfile in your working dir, so do pipenv shell BEFORE copying .venv

## Currently animate is the only available action
## Morph module
- python -m cadwalk animate -g sphere -a morph_single -d 30 30 3
- keyboard 'r': morph result

## Construct module
This is a experimental module to create cradle toy objects for cadwalk to play and memorize.
Construct module allowes user and cadwalk to alter these objects using keyboard and mouseWheel.
Constructed objects can be rotated, exported or saved. Video can be recorded.
### Construct waitKeys parameters, most work better for dim 1
#### Run cadwalk
- python -m cadwalk animate -g donut -a construct_single -d 40 60 3

#### then do
- NOTE: some of the actions below are only available for certain shapes 
- check geometries.params file, if all parameter are zero, they will not change by keyboard
- keyboard 'space': hold/run rotation
- keyboard 'e': show/hide edges
- keyboard 'f': show/hide faces
- keyboard 'p': verteces/points
- keyboard 'c':         close/open mesh dim 0
- keyboard shift+'c':   close/open mesh dim 1
- keyboard 'a':         use mouseWheel to change rotation angles dim 0
- keyboard shift+'a':   use mouseWheel to change rotation angles dim 1
- keyboard 'o':         use mouseWheel to change point offset dim 0
- keyboard shift+'o':   use mouseWheel to change point offset dim 1

### For wave feature try different geometry, i.e. disk, flag
- keyboard 'w':         use mouseWheel to change sin wave distortion dim 0
- keyboard shift+'w':   use mouseWheel to change sin wave distortion dim 1

## Simple constructor Example:
- python -m cadwalk animate -a construct_single -g donut -d 40 60 3
- keyboard 'e': show/hide edges
- press space multiple times and view changes
- press 'a' >> then turn mouseWheel until you see a horizontally cut donut
- press 'g'
- press 'c' >> press repeatedly and view changes
- press shift+'a' >> then turn mouseWheel until you see a vertically cut donut
- press 'g'
- press shift+'c' >> press repeatedly and view changes
- press 's' >> exports verteces as stl file. NOTE: onError create a stls folder
- press shift+'s' >> saves params as reloadable yml file: NOTE: onError create a ymls folder
- try other key combinations from available waitKeys


# Arguments
## -g: Available geomTypes (see geometries.params.py known_objects):
- disc
- sphere
- donut
- spiral
- helix
- flag
- pipe
- peanut

### NOTE: load model from .yml file uses -g full_Path_to.yml
- You can save/create a valid yml file from a running animation by pressing Shift+s
- The yml file will be saved into cadwalk.media.npys
- To load the geometry form yml file use: -g /full_Path_to.yml

## -i Available images:
NOTE:
- to use -i you first have to set settings.py sts.imgPath directory and add image.png
- call the image like: -i imgName, NOTE: cadwalk gets image by image.startswith(imgName)

## -r Record Video:
- make sure media/videos directory exists, else create it
- run with -r flag (no parameters)

## -v Verbose:
- run with -v integerValue from 1 to -inf

## -s scaler:
- run with -s integerValue from 1 to -inf (best result is arround sceneShape.min() // 3)

## -d dims:
- number of vertex points per rotational dim ...
- a donut with i.e. dims=[40, 60, 3] would have 60 rings wiht 40 vtx per ring
- NOTE: dims[-1] must be 3 for 'BGR'

# Important .py files
## settings.py
- sts.mediaPath: NOTE: you need directories: images, npys, stls, videos

### params.py
known_objects: dict
- contains arays of shape (2, 5, 3) with each being a pre-configured object, see -g argument
    - each geometry is created using two rotation steps 0 and 1
    - each step has 5 parameter (rotation-angles, point-offsets, cos-wave-patterns, twists, shifts)

    - for angles, offsets, twists, shifts there are 3 eucledian axis i.e. (y, x, z)
    - for cos-wave there are (frequency, amplitude, wave-offset) ...