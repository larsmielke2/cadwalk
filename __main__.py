"""
    __main__.py from geometry package
"""
import os, sys
import time

from cadwalk.code import arguments
# resources
from cadwalk.code.resources import module_imports
from cadwalk.code.resources import contract


def main(*args, moduleName, **kwargs):
    """
        takes arguments and pipes them into cadwalk
        Example: python -m cadwalk animate -a construct -g donut
        cadwalk: package name
        construct: action to be run, is a module like cadwalk/code/construct.py
        for arguments run python -m cadwalk --help or info
    """
    modulePath = os.path.join(os.path.split(os.path.abspath(__file__))[0],
                                                        'code', moduleName + '.py')
    module = module_imports.import_module(filePath=modulePath)

    getattr(module, 'main')(*args, **kwargs)
    if module == 'info': return True

kwargs = arguments.mk_args().__dict__
if not kwargs.get('allYes'):
    kwargs.update(contract.check_kwargs(**kwargs))
kwargs.update(contract.update_kwargs(**kwargs))
if kwargs['verbose']:
    print(f"\n__main__.py running main with kwargs: \n{kwargs}")
main(**kwargs)