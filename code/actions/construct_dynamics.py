import numpy as np
# package internal imports
from cadwalk.code.geometries import im_ex_port
from cadwalk.code.renderer.waitkeys import add_waitKeys
import cadwalk.code.geometries.params as pars
import cadwalk.code.resources.settings as sts


def run( s:object, r:object, d:object, *args, imgColors:bool=False, **kwargs ):
    """
        call like:
        python -m cadwalk animate -a construct_dynamics -i planets -s 1080 1920 3 -d 30 50
    """
    # local runntime scene paramters
    add_waitKeys('constructors', 'single', *args, r=r, **kwargs)
    zoomFactor = .5
    move, cRads = True, np.array(((0/1, 10/3, 1/2), (0/1, 10/3, 1/2)), dtype=np.float32)

    d.pos[:, :2] = r.center
    d.pos[:, 0] += (150., -100)
    # endles Render loop innitiated
    while r.running:
        if r.waitKey == 'p':
            s.points = not s.points
            s.verteces = not s.points
        elif r.waitKey == 'e':
            s.edges = not s.edges
        elif r.waitKey == 'f':
            s.faces = not s.faces
        d.geoms[1].params[0, 2, 2] -= 5e-3
        d.geoms[1].mk_geometry(*args, **kwargs)
        cRads[:, 2] += (1e-2, -1e-2)
        d.cRads[:] = cRads
        d.pos[0, :2] -= np.array((1/22, 1/5)) *np.array(
                                                            (np.sin(r.numFrames / 40), 
                                                            np.cos(r.numFrames / 40)), 
                                                        dtype=d.pos.dtype) * 100
        d.zoomFactors[:] = np.array(((0.25, 0.25, 0.25), (.8, .8, .8)))
        d.zoomFactors[0] *= +(np.cos(r.numFrames / 40) * 1/2) + 1.2
        # d.zoomFactors[0] += d.pos[0, 2]
        d.update(*args, **kwargs)
        # if not image: d.set_color('basic_color')
        s.draw(d.rotateds, d.colors, [gm.mesh for gm in d.geoms])
        r.render(s.canvas)

