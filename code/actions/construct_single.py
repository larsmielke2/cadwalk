import numpy as np
# package internal imports
from cadwalk.code.geometries import im_ex_port
from cadwalk.code.renderer.waitkeys import add_waitKeys
import cadwalk.code.geometries.params as pars
import cadwalk.code.resources.settings as sts


def run( geometries:list, s:object, r:object, d:object, *args,
                # geometry, scene, renderer
                image=None, imgColors:bool=False, **kwargs ):
    """
        animates a constructor for the shape
        i.e. pressing a or A will enable the mousWhell slicing for the shape
            pressing o or = will change the offset between dim0 and dim1
            pressing w or W will add waves to dim0 or dim1
    """
    g = geometries[0]
    add_waitKeys('constructors', 'single', *args, r=r, **kwargs)
    cyclic, params = g.cyclic.copy(), g.params.copy()
    zoomFactor = 1.
    move, cRads = True, np.array((0/1, 10/3, 1/2), dtype=np.float32)
    # endles Render loop innitiated
    while r.running:
        if r.mouseDown and not r.stateKey == 'space':
            cRads[1:] = (r.mousePoints[r.mouseIx] - r.center[0]) / 50
        
        # specific (paramIndexes) are adjusted depending on active stateKey like key_pIx
        sk, _, ix = r.stateKey.partition('_')
        if ix:
            ix = int(ix)
            params[ix, pars.pIx[sk]] = g.params[ix, pars.pIx[sk]] * r.mouseWheelState
        else:
            zoomFactor = r.mouseWheelState
        
        # stqteless one time actions only apply ones a key is pressed
        if r.waitKey == 'hold':
            move = not move
        elif r.waitKey == 's':
            # indexing mesh for export to stl file
            im_ex_port.export_stl_mesh( np.flip(d.vtx[0][d.geoms[0].mesh], -1 ), 
                                    fileName=f"{d.geoms[0].name}_{str(d.geoms[0].gId)}")
        elif r.waitKey == 'S':
            im_ex_port.export_yml(g, params)
        elif r.waitKey in ['c', 'C']:
            ix = 1 if r.waitKey == 'C' else 0
            cyclic[ix] = not cyclic[ix]
            g.mk_mesh(cyclic=cyclic)
        elif r.waitKey == 'p':
            s.points = not s.points
            s.verteces = not s.points
        elif r.waitKey == 'e':
            s.edges = not s.edges
        elif r.waitKey == 'f':
            s.faces = not s.faces
        g.mk_geometry(params=params, **kwargs)
        # move and scale generated geometry
        if move and not r.stateKey == 'space': cRads[2] += 1e-2
        d.cRads[g.ix] = cRads
        d.pos[:, :2] = r.center
        d.zoomFactors[g.ix] = zoomFactor
        d.update(*args, **kwargs)
        if not image: d.set_color('basic_color', g.ix)
        s.draw(d.rotateds, d.colors, [gm.mesh for gm in d.geoms])
        r.render(s.canvas)

