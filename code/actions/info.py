# animate.py
from cadwalk.code.geometries.geometries import Geometry
from cadwalk.code.scenes.drawing import Scene
from cadwalk.code.dynamics.obj_dynamics import Dynamics

import cadwalk.code.geometries.params as pars
import cadwalk.code.resources.settings as sts

def main(*args, **kwargs):
    print(f"use kwargs: \n{kwargs}")
    print('Examples: NOTE you have to download images to work with -i parameter')
    print(f"\tpython -m cadwalk animate -a construct_single -g donut")
    print(f"\tpython -m cadwalk animate -a morph_single -g sphere -i earth")
    print(f"\tpython -m cadwalk animate -a morph_single -g sphere -i 390px-Blue_Marble")