import numpy as np
# package internal imports
from cadwalk.code.geometries import im_ex_port
from cadwalk.code.renderer.waitkeys import add_waitKeys
import cadwalk.code.geometries.params as pars

def run(    geometries:list, s:object, r:object, d:object,
                *args,
                # geometry, scene, renderer
                image=None, imgColors:bool=False, **kwargs):
    """
        animates a transformation of a single shape
        i.e. pressing r will morph the shape
            pressing d will show the drawing process for the shape
    """
    g = geometries[0]
    add_waitKeys('animators', 'single', *args, r=r, **kwargs)
    cyclic, params = g.cyclic.copy(), g.params.copy()
    zoomFactor = 0.6
    move, cRads = True, np.array((0/1, 10/3, 1/2), dtype=np.float32)
    dRad = 2e-2
    # endles Render loop innitiated
    while r.running:
        if r.mouseDown:
            cRads[1:] = (r.mousePoints[r.mouseIx] - r.center[0]) / 50
        # specific (paramIndexes) are adjusted depending on active stateKey like key_pIx
        sk, _, ix = r.stateKey.partition('_')
        if ix and not sk in ['rotation', 'draw']:
            ix = int(ix)
            params[ix, pars.pIx[sk]] = g.params[ix, pars.pIx[sk]] * r.mouseWheelState
        elif sk == 'rotation':
            params[0, 0, 0] += 1e-2
            params[1, 1, 1] += 5e-4
            zoomFactor *= 1.0009
            dRad *= 1.01
        elif sk == 'draw':
            if r.numFrames * 1e-2 <= 2*np.pi:
                params[0, 0, 0] = r.numFrames * 1e-2
                params[1, 0, 2] = r.numFrames * 0.
            elif params[1, 0, 2] <= 2*np.pi:
                params[1, 0, 2] = r.numFrames * 1e-2 - 2*np.pi
                d.colors[:, -1] = r.numFrames // 5
                d.colors[:, -2] = 255 - r.numFrames // 5
                imgColors = True
            elif d.colors.min() < 245:
                dCol = (255 - d.colors) // 30
                d.colors += np.where(dCol>0, dCol, 1) 
            elif d.colors.min() < 255:
                dCol = (255 - d.colors) // 30
                d.colors += np.where(dCol>0, dCol, 1) 
                s.faces = True
            else:
                r.bgrColor = 255 if r.numFrames % 3 else 0
            zoomFactor = 1.
            dRad += 1e-5
        else:
            zoomFactor *= 1.001
        # stqteless one time actions only apply ones a key is pressed
        if r.waitKey == 'hold':
            move = not move
        elif r.waitKey == 's':
            im_ex_port.export_stl_mesh(g, fileName=g.name)
        elif r.waitKey == 'S':
            im_ex_port.export_yml(g, params)
        elif r.waitKey in ['c', 'C']:
            ix = 1 if r.waitKey == 'C' else 0
            cyclic[ix] = not cyclic[ix]
            g.mk_mesh(cyclic=cyclic)
            r.mesh = g.mesh
        elif r.waitKey == 'e':
            s.edges = not s.edges
        elif r.waitKey == 'f':
            s.faces = not s.faces
        g.mk_geometry(params=params, **kwargs)
        # move and scale generated geometry
        if move and not r.stateKey == 'space': cRads[2] += 1e-2
        d.cRads[g.ix] = cRads
        d.pos[g.ix, :2] = r.center
        d.zoomFactors[g.ix] = zoomFactor
        d.update(g.ix, *args, **kwargs)
        if not image: d.set_color('basic_color', g.ix)
        s.draw(d.rotateds, d.colors, [gm.mesh for gm in d.geoms])
        r.render(s.canvas)