# animate.py
"""
    This module is called by the moduleName argument from arguments.py
    Entry script called via __main__.py
    Creates all relevant objects for running a geometry animation and adds them to kwargs.
    Then runs action.run(*args, **kwargs) as specified by -a keyWordArgument
"""
import os
from cadwalk.code.geometries.geometries import Geometry
from cadwalk.code.dynamics.colors import Colors
from cadwalk.code.scenes.drawing import Scene
from cadwalk.code.dynamics.obj_dynamics import Dynamics
from cadwalk.code.renderer.renderer import Render

from cadwalk.code.resources import module_imports
import cadwalk.code.resources.settings as sts
import cadwalk.code.geometries.params as pars


def mk_geometries(*args, numObjs, dims, image, **kwargs):
    """
        setup geometry, geometries obj-instances to be displayed
    """
    # some dynamics contain params pre defined images
    images = pars.dynamics_images.get(image, [image for _ in range(numObjs)])
    # loop over images to create geometries
    geometries = []
    for i, image in enumerate(images):
        imgColors = Colors(*args, **kwargs).colorize(*args, image=image, dims=dims, **kwargs)
        g = Geometry(*args, colors=imgColors, dims=imgColors.shape, **kwargs)
        g.mk_geometry(*args, **kwargs)
        g.mk_mesh()
        geometries.append(g)
    return geometries

def mk_dynamics(*args, geometries, image, geomType, **kwargs):
    d = Dynamics(image if image else geomType, *args, **kwargs)
    for g in geometries:
        d.add_geoms(g, *args, **kwargs)
    return d

def construct(*args, **kwargs):
    kwargs.update({'geometries': mk_geometries(*args, **kwargs)})
    kwargs.update({'numObjs': len(kwargs['geometries'])})
    kwargs.update({'d': mk_dynamics(*args, **kwargs)})
    kwargs.update({'s': Scene(*args, **kwargs)})
    kwargs.update({'r': Render(*args, name=kwargs['d'].name, **kwargs)})
    return kwargs

def main(*args, action, **kwargs):
    """
        EXAMPPLE: python -m cadwalk animate -a construct_single -g donut
        __main__.py entry point for running construct animations
    """
    kwargs = construct(*args, **kwargs)
    actionPath = os.path.join(sts.basePath, 'actions', action + '.py')
    module_imports.import_module(filePath=actionPath).run(*args, **kwargs)