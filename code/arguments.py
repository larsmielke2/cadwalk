import argparse

def mk_args():
    """ 
        run like: kwargs = arguments.mk_args().__dict__
    """
    parser = argparse.ArgumentParser(description="Provide --t image type such as npy!")
    
    parser.add_argument('moduleName', metavar='moduleName', nargs='?', default='animate',
                                                            help='module to run i.e. animate'
                                                            )

    parser.add_argument('-a', '--action',  required=False, nargs=None,
                                                type=str, default='construct_single',
                                                help='tell cadwalk to perform an action i.e. construct'
                                                )

    parser.add_argument('-g', '--geomType', required=False, nargs=None,
                                                type=str, default='sphere',
                                                help='i.e. donut, sphere or path_to_yml_file'
                                                )

    parser.add_argument('-i', '--image', required=False, nargs=None,
                                                type=str, default=None,
                                                help='image name.startswith, for geometry'
                                                )

    parser.add_argument('-s', '--sceneShape', required=False, nargs='+',
                                                type=int, default=(800, 800, 3), 
                                                help='size sceneShape of created geometry i.e. 50'
                                                )

    parser.add_argument('-n', '--numObjs', required=False, nargs=None,
                                                type=int, default=1, 
                                                help='num of geometries i.e. 50'
                                                )

    parser.add_argument('-d', '--dims', required=False, nargs='+',
                                                type=int, default=[40, 60, 3], 
                                                help='number of verteces per dimension'
                                                )
    # MOT YET IMPLEMENTED
    """
        parser.add_argument('-p', '--savePath', required=False, nargs=None,
                                                    type=str, default=None, 
                                                    help="savePath: You can use 'default'"
                                                    )

    """
    parser.add_argument('-r', '--record',  required=False, nargs='?' , const=1,
                                                type=bool, default=False, 
                                                help='record render result as mp4'
                                                )
    parser.add_argument('-y', '--allYes',  required=False, nargs='?' , const=1,
                                                type=bool, default=False, 
                                                help='allYes render wihout checks'
                                                )
    parser.add_argument('-v', '--verbose', required=False, nargs=None,
                                                type=int, default=0, 
                                                help='verbosity i.e. 1, 2, 3'
                                                )

    return parser.parse_args()