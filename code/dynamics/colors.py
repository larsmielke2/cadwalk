# colors.py
import numpy as np
import os
# package internal imports
import cadwalk.code.resources.settings as sts
from cadwalk.code.resources import np_collections as nc


class Colors:

    def __init__(self, *args, **kwargs):
        pass

    def colorize(self, image=None, dims=None, *args, **kwargs):
        # get a equirectangular shape for defining spherical shape
        # if no image is found, np.full is used to create default image using -d dims argument
        if image:
            images = [im for im in os.listdir(sts.imgPath) if \
                    (os.path.isfile(os.path.join(sts.imgPath, im)) and im.startswith(image))]
        else:
            images = None
        if images:
            imgArray = nc.load_image(os.path.join(sts.imgPath, images[0]), dims)
        else:
            imgArray = np.full(dims, 255, dtype=np.int32)
        return imgArray