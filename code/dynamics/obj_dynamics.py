# single_obj_dynamics.py
# setup_geometry.py
import numpy as np
import os
from collections import defaultdict
# package internal imports
from cadwalk.code.geometries.geometries import Geometry
from cadwalk.code.resources import np_collections as nc
from cadwalk.code.resources import rotations as npr

import cadwalk.code.resources.settings as sts

class Dynamics:

    def __init__(self, name, numObjs, *args, dims, **kwargs):
        self.name = os.path.basename(name)[:-20] if '.yml' in name else name
        self.numObjs = numObjs
        # geom paramter collections for dynamic
        # self.vtx = np.zeros((numObjs, np.prod(dims[:2]), 3), dtype=sts.floats)
        self.geoms = [None for i in range(numObjs)]
        # self.vtx must be list because np.array copies hence looses connection to g.vtx
        self.vtx = [None for i in range(numObjs)]
        self.rotateds = np.zeros((numObjs, np.prod(dims[:2]), 3), dtype=sts.floats)
        self.colors = np.zeros(self.rotateds.shape, dtype=sts.uInts)
        self.baseColors = self.colors.copy()
        self.pos = np.zeros((numObjs, 3), dtype=self.rotateds.dtype)
        self.vels = np.zeros((numObjs, 3), dtype=self.rotateds.dtype)
        self.scalers = np.full((numObjs, 3), 50, dtype=self.rotateds.dtype)
        self.zoomFactors = np.full((numObjs, 3), 1., dtype=self.rotateds.dtype)
        self.cRads = np.full((numObjs, 3), 1e-1, dtype=self.rotateds.dtype)
        self.dRads = np.full((numObjs, 3), 0., dtype=self.rotateds.dtype)
        self.all = slice(None, None, None)

    def add_geoms(self, g, *args, ix=None, **kwargs):
        # append and concatenate did not work because reference to geometry is lost
        ix = ix if ix else g.ix
        self.geoms[ix] = g
        self.vtx[ix] = g.vtx
        self.colors[ix] = g.colors
        self.baseColors[ix] = np.abs(self.vtx[ix] / self.vtx[ix].max(0) * 250)
        self.pos[ix] = g.pos
        self.vels[ix] = g.vel
        self.cRads[ix] = g.cRad
        self.scalers[ix] = g.scaler
        g.dynamic = self.name

    def update(self, ix=None, *args, **kwargs):
        ix = ix if ix else self.all
        self.cRads[ix] += self.dRads[ix]
        self.rotateds = np.array(npr.rotate3ds(
                                                self.vtx[ix], 
                                                self.cRads[ix]), dtype=self.rotateds.dtype)
        self.rotateds[ix] *= (self.scalers[ix] * self.zoomFactors[ix])[:, None, ...]
        self.rotateds[ix] += self.pos[ix][:, None, ...]

    def set_color(self, color, ix=None, *args, **kwargs):
        ix = ix if ix else self.all
        if isinstance(color, np.ndarray):
            self.colors[ix] = color
        else:
            self.colors[ix] = self.baseColors[ix]