#geometries.py
import datetime as dt
from itertools import count
import numpy as np
import os, re

from cadwalk.code.resources import rotations as npr
from cadwalk.code.resources import np_collections as nc
from cadwalk.code.geometries.im_ex_port import import_yml
import cadwalk.code.geometries.params as pars
import cadwalk.code.resources.settings as sts


class Geometry:
    _ix = count(0)
    """
        Serves as both a Geometry and Geometries class
        creates a geometry instance which can be added to a g.geometries object
        mk_geometry crates the geometry and mk_group adds it to a geometries object
        g can hold multiple g.geometries objects, so super objects can be created
    """

    def __init__(self, *args,
                    geomType:str='Main', 
                    dims:tuple=None,
                    colors:np.ndarray=None, # if tuple or list -> np.array(colors)
                    cRad:np.ndarray=(0., 0., 0.),
                    scaler:np.ndarray=[200, 200, 200],
                    cyclic:list=[False, False],
                    **kwargs
                    ):
        """
            takes a colors array and generates (cumRotates) a shape based on that 
            array dimensions + params
            NOTE:   colors has to be a image like array. i.e. shape=(28, 28, 3?)
                    for a sphere a equirectangular image should be used
            params are in params.py file and define the angles of rotation to genereate the
            Geometry
            Example: see ./setup_geometry.py
        """
        # general parameters
        self.name = geomType
        self.gId = re.sub(r"([:.])", r"-" , str(dt.datetime.now()))
        self.ix, self.dynamic = next(self._ix), None
        # vtx dimensions
        self.dims = dims
        self.cDims = np.array([np.prod(self.dims[:i]) for i in range(3)], dtype=np.int32)
        # base index arrays for both dimensions are created counting from 0 to len(dim)
        self.ax1 = np.tile(np.arange(self.dims[0], dtype=sts.ints)[..., None], (1, 3))
        self.ax2 = np.tile(np.arange(self.dims[1], dtype=sts.ints)[..., None], (1, 3))
        # vtx parameters
        self.mk_params(geomType, *args, **kwargs)
        self.vtx = np.zeros((self.cDims[-1], 3), dtype=sts.floats)
        self.colors = self.apply_colors(colors, *args, **kwargs)
        self.cRad = cRad if isinstance(cRad, np.ndarray) else np.array(cRad, sts.floats)
        self.scaler = scaler if isinstance(scaler, np.ndarray) else np.array(scaler, sts.floats)
        self.pos = np.zeros((3), dtype=sts.floats)
        self.vel = np.zeros((3), dtype=sts.floats)
        # mesh parameters
        self.cyclic = cyclic
    

    def apply_colors(self, colors, *args, **kwargs):
        if isinstance(colors, np.ndarray):
            return np.stack(colors, axis=1).reshape(-1, 3).astype(sts.uInts)
        else:
            return np.array(((255, 255, 255), ), dtype=sts.uInts)

    def mk_params(self, geomType, *args, **kwargs):
        params = pars.known_objects.get(geomType)
        if params is None: self.name, params = import_yml(geomType)
        outPars = params.copy()
        outPars[:, 0] *= (2*np.pi) # all angles are multiples of 2*PI
        self._params = outPars

    @property
    def params(self):
        return self._params

    def mk_geometry(self, *args, params=None, **kwargs) -> np.ndarray:
        params = params if isinstance(params, np.ndarray) else self.params
        """
            makes a geometry by rotating a point for n dims using params
            NOTE, params and dims are instance paramter. see __init__ doc string 
            call like: g.mk_geometry(*args, **kwargs)
        """
        self.vtx[0] = 0.
        # fill self.vtx by recursive rotation
        for axis, (dim, par) in enumerate(zip(self.dims, params), start=1):
            # create a shape for each dimension by rotating a point or sub-shape
            npr.rotate_3d_cum( 
                                self.vtx[:self.cDims[axis]],      # target
                                self.vtx[:self.cDims[axis-1]],    # source
                                # rotation angles for all rotation steps for dim i
                                np.linspace( (0., 0., 0,), par[0], dim ).astype(sts.floats),
                                par[1]
                            )
            self.transform(axis, dim, par)
        self.vtx[:] = nc.normalize(self.vtx)

    def transform(self, axis, dim, par):
        # add a cos wave onto dimension
        if np.any(par[2] != 0):
            self.vtx[:self.cDims[axis]] *= nc.mk_wave(self.cDims[axis], *par[2])[..., None]
        if np.any(par[3] != 0):
            self.vtx[:] = npr.experimental_twist(self.vtx, self.dims[:2], par[3, 2])
        if np.any(par[4] != 0):
            stretch = np.tile(np.arange(0, self.dims[1])[..., None], (1, self.dims[0]))
            self.vtx[:, 0] += (stretch.flatten() / par[4, 2]).astype(sts.floats)
            self.vtx[:] = nc.normalize(self.vtx)


    def mk_mesh(self, *args, cyclic=None, **kwargs):
        """
            mesh.shape = (n, 1, d) for n looped graphs of len d
            i.e. if mesh contains 5 triangles then mesh.shape = (5, 1, 3)
            cyclic decides whether graph is cyclic graph or incomplete graph for (x, y) edges
            incomplete graphs leave a gap, which might be intended for an open shape
            0 mesh direction:     South >> West  >> NorthEast
            1 mesh direction:     West  >> North >> SouthEast

        """
        cyclic = cyclic if cyclic else self.cyclic
        mesh = []
        # there are self.ax2 triangles per rectangle, so self.ax2 loops to create the mesh
        for i in range(2):
            # base index arrays for both dimensions are created counting from 0 to len(dim)
            ax1, ax2 = self.ax1.copy(), self.ax2.copy()
            # each dimension uses np.roll to shift index to line target
            # NOTE: column 0 always keeps original vtx index. There are ax2 triangles per vtx.
            # So vtx[index] == mesh[index] and vtx[index] * 2 also == mesh[index]
            if i == 0:
                ax1[:, 1] = np.roll(ax1[:, 1], 1, 0)
                ax2[:, 2] = np.roll(ax2[:, 2], -1, 0)
            else:
                ax1[:, 2] = np.roll(ax1[:, 2], -1, 0)
                ax2[:, 1:] = np.roll(ax2[:, 1:], -1, 0)
            triangles = ax1[None, ...] + ax2[:, None, ...] * self.dims[0]
            # if shape is open either in x or y dim, then ending triangles are rolled back
            # to arive at origin
            if not cyclic[0]:
                if i == 0:
                    triangles[:, 0, 1] = triangles[:, 0, 0]
                else:
                    triangles[:, -1, 2] = triangles[:, -1, 0]
            if not cyclic[1]:
                    triangles[-1, :, -1] = triangles[-1, :, 0]
                    triangles[-1, :, -2] = triangles[-1, :, 0]
            
            mesh.append(triangles.reshape(-1, 3))
        self.mesh = np.concatenate(mesh, axis=0)
        return self.mesh

    def __str__(self):
        return f"{self.name, self.gId}, vtx.shape={self.vtx.shape}"