# io.py
import numpy as np
import os, yaml
import cadwalk.code.resources.settings as sts


def export_stl_mesh(*args, fileName:str, **kwargs):
    """
        exports geoms mesh as stl file
        call like:  im_ex_port.export_stl_mesh( np.flip(d.vtx[0][d.geoms[0].mesh], -1 ), 
                                    fileName=f"{g.name}_{str(g.gId)}")
    """
    content = mk_stl_mesh(*args, **kwargs)
    exportPath = os.path.join(sts.mediaPath, 'stls', f"{fileName}.stl")
    with open(exportPath, 'wb') as f:
        f.write(content.encode('ascii'))
    print(f"stl saved to exportPath: \n{exportPath}")

def mk_stl_mesh(mesh, *args, **kwargs):
    bounds = f"solid Exported from geometries.py" + '\n'
    content = bounds
    for triangle in mesh.reshape(-1, 3, 3):
        content += ('facet normal 0.000000 0.000000 1.000000' + '\n')
        content += ('outer loop' + '\n')
        for point in triangle:
            content += (f"vertex {' '.join([f'{t:6f}' for t in point])}" + '\n')
        content += ('endloop' + '\n')
        content += ('endfacet' + '\n')
    content += ('end' + bounds)
    return content

def export_yml(g, params=None, *args, fileDir=f"{sts.mediaPath}/npys", **kwargs):
    expParams = params.copy() if isinstance(params, np.ndarray) else g.params.copy()
    expParams[:, 0] /= (2*np.pi)
    fileName = f"{g.name}_{g.gId}.yml"
    with open(os.path.join(fileDir, fileName), 'w') as f:
        out = {fileName: expParams.tolist()}
        f.write(yaml.dump(out))
    print(f"success saving: {fileName} to {fileDir}")

def import_yml(path, *args, **kwargs):
    with open(path, 'r') as f:
        pars = yaml.safe_load(f)
        name = list(pars.keys())[0]
        params = np.array(pars[name], dtype=sts.floats)
    return name[:-20], params

def import_shape(g, *args, importPath:str, **kwargs):
    # not tested: can be used to import stl files
    import struct
    with open(importPath, 'rb') as f:
        temp=''
        for j in range(40):
            temp='facet normal: '
            for i in range(3):
                temp = temp + str(round(struct.unpack('f',f.read(4))[0],6)) + ' '
            print(temp)
        
            for n in range(3):
                temp='vertex: ' 
                for i in range(3): 
                    temp = temp + str(round(struct.unpack('f',f.read(4))[0],6)) + ' ' 
                print(temp)
            attr=f.read(2)
            print('###################################')
    temp=temp[:-1]
    print(temp)