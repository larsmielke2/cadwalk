# orbit_controlls.py
import numpy as np
from cadwalk.code.resources.rotations import rotate3d

def basic_orbit_controlls(  vtxs, r, *args, 
                            cumRads=np.zeros((3), dtype=np.float32), 
                            basicRot=None, 
                            **kwargs):
    """
        takes a normalized (zero centered !) vtx array and returns a rotated version
        this has to be called before adding the position to the object (zero centered !)
        rotation agles are adjusted if mouse is leftDown
        basicRot is optional, if not provided there is no self rotation
        zoom multiplier is added from r.mouseWheelState

        call like:
                from cadwalk.code.renderer.renderer import Render
                r = Render(sceneShape=(500, 500, 3))
                vtxs = any array of shape (n, 3)
                rotateds = basic_orbit_controlls(vtxs, r, basicRot=np.array of shape 3,)

    """
    if r.leftDown:
        cumRads[1:] = ((r.mousePoint - r.center) / (r.sceneShape[0] / 4))
    elif (basicRot is not None) and (r.stateKey != 'space'):
        cumRads += basicRot
    rotateds = rotate3d(vtxs, cumRads) * r.mouseWheelState
    rotateds[:, :2] += r.center
    return rotateds