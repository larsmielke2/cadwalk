# drawing.py
import cv2
from itertools import cycle
import numpy as np
# from numba import njit, prange
import time
import datetime as dt

import os, re, sys
import cadwalk.code.resources.settings as sts

np.set_printoptions(edgeitems=30, linewidth=100000, suppress=True)

class Render:

    def __init__(self, *args,
                    name:str='main',
                    sceneShape,
                    record:bool=False,
                    verbose:int=0,
                    **kwargs
                ):
        """
            takes a (n, 3) array and renders it into a canvas
            call lke: Render(sceneShape=sceneShape, rotated=g.vtx.copy(),
                                colors=colors, mesh=mesh, edges=True, faces=Tru
        """
        # session elements
        self.name = os.path.basename(name)[:-20] if '.yml' in name else name
        self.verbose = verbose
        self.sessionId = re.sub(r"([:.])", r"-" , str(dt.datetime.now()))
        # setup window elements
        self.sceneShape = np.array(sceneShape[:2], dtype=sts.ints)
        self.center = self.sceneShape // 2
        self.msgColor = (255, 255, 255)
        self.mk_cv2Window(*args, **kwargs)
        # boolean parameters for display
        # parameter for mouse actions/click actions
        self.mousePoints, self.mouseColors = np.zeros((100, 2), dtype=sts.ints), (255, 255, 0)
        self.mouseIxs = cycle(range(self.mousePoints.shape[0]))
        self.mouseIx = next(self.mouseIxs)
        self.mouseWheelState, self.down, self.up = 1., -.1, .1
        self.leftDown, self.mouseDown = False, False
        # parameter for keyboard interactions
        self.waitKey, self.waitKeyDelay = -1, 1
        self.stateKey, self.lastStateKey = 'running', False
        self.waitKeys = {13: 'running', 113: 'Quit', 115: 's', 83: 'S', 32: 'space'}
        self.waitKeyParams =  {}
        # stateless keys can be pressed any time and do not change stateKey
        self.stateLessKeys, self.stateLessPressed = ['s', 'S'], None
        self.running, self.numFrames = True, 0
        # record video setup part
        self.videoPath = self.mk_video(record, *args, **kwargs)
        # comments are displayed using the follwoing parameter
        self.msgDist = self.sceneShape[::-1] // 25
        self.msgSize = (self.sceneShape[1] / 3000) + .4
        # lets roll
        self.startTime = time.time() + 1e-6

    def mk_cv2Window(self, *args, **kwargs):
        cv2.namedWindow(self.name, cv2.WINDOW_NORMAL)
        cv2.startWindowThread()
        cv2.setMouseCallback(self.name, self.mouse_listen)
        return False

    def mk_video(self, record, *args, geomType=None, **kwargs):
        if record:
            videoPath = os.path.join(sts.mediaPath, 'videos')
            outFps, outPath = 25, os.path.join(videoPath, f'{self.sessionId}_{self.name}.mp4')
            self.video = cv2.VideoWriter(
                                        outPath, 
                                        cv2.VideoWriter_fourcc(*'mp4v'), 
                                        outFps, 
                                        tuple(np.flip(self.sceneShape)), 
                                        )
            print(f"render.mk_video fps: {outFps}, to: {outPath}")
        else:
            videoPath = None
        return videoPath

    def mouse_listen(self, event, x, y, flags, param):
        self.mouseIx = next(self.mouseIxs)
        if event == cv2.EVENT_LBUTTONUP:
            self.leftDown, self.mouseDown = False, False
        elif event == cv2.EVENT_LBUTTONDOWN or self.leftDown:
            self.mousePoints[self.mouseIx] = (y, x)
            self.leftDown, self.mouseDown = True, True
        elif event == cv2.EVENT_MOUSEWHEEL:
            #sign of the flag shows direction of mousewheel
            if flags > 0:
                self.mouseWheelState += self.up
            else:
                self.mouseWheelState += self.down
        self.mouseWheelState = max(-100, self.mouseWheelState)

    def add_comment(self, canvas, *args, msg=None, **kwargs):
        td = time.time() - self.startTime
        m = f"name: {self.name}, fps: {self.numFrames // td} "
        m2 = f"key: {self.stateKey}, zoom: {self.mouseWheelState:.2f}"
        cv2.putText(canvas, m + m2 + (msg if msg else ''),
                        self.msgDist,
                        cv2.FONT_ITALIC,
                        self.msgSize,
                        self.msgColor,
                        2,
                    )

    def waitKey_state(self, *args, **kwargs):
        if self.verbose: print(f"Render.waitKey_state detected waitKey: {self.waitKey}")
        if self.waitKey in self.stateLessKeys:
            self.stateLessPressed = self.waitKey
        else:
            if self.stateKey == self.waitKey:
                self.waitKey = self.lastStateKey
            self.waitKeyParams[self.stateKey] = [self.mouseWheelState, self.down, self.up]
            self.mouseWheelState, self.down, self.up = \
                                        self.waitKeyParams.get(self.waitKey, (1., -.1, .1))
            self.lastStateKey = self.stateKey
            self.stateKey = str(self.waitKey)

    def update_runntime_state(self, *args, **kwargs):
        if self.waitKey == 'Quit':
            cv2.destroyAllWindows()
            if self.videoPath: self.video.release()
            self.running = False
        else:
            self.numFrames += 1

    def handle_key_press(self, *args, **kwargs):
        key = cv2.waitKeyEx(self.waitKeyDelay)
        self.waitKey = self.waitKeys.get(key, key)
        if self.waitKey != -1: self.waitKey_state(*args, **kwargs)

    def draw(self, canvas, *args, **kwargs):
        if self.mouseDown:
            canvas[tuple(self.mousePoints.T)] = self.mouseColors

    def render(self, canvas, *args, **kwargs):
        self.handle_key_press(*args, **kwargs)
        self.update_runntime_state(*args, **kwargs)
        self.draw(canvas, *args, **kwargs)
        if self.videoPath: self.video.write(canvas.astype(sts.uInts))
        self.add_comment(canvas, *args, **kwargs)
        cv2.imshow(self.name, canvas)

    @property
    def mousePoint(r):
        return r.mousePoints[r.mouseIx]