# services.py

waitKeySets = {
                'basics': {
                                        99: 'c',
                                        67: 'C',
                                        101: 'e',
                                        102: 'f',
                                        112: 'p',
                                },
                'constructors': {
                                        97: 'angles_0',
                                        65: 'angles_1',
                                        111: 'offsets_0',
                                        79: 'offsets_1',
                                        119: 'waves_0',
                                        87: 'waves_1',
                                        105: 'inclines_0',
                                        73: 'inclines_1',
                                        99: 'c',
                                        67: 'C',
                                        104: 'hold',
                                        101: 'e',
                                        102: 'f',
                                        112: 'p',
                                },
                'animators': {
                                        114: 'rotation_0',
                                        82: 'rotation_1',
                                        97: 'angles_0',
                                        65: 'angles_1',
                                        100: 'draw_0',
                                        68: 'draw_1',
                                        111: 'offsets_0',
                                        79: 'offsets_1',
                                        119: 'waves_0',
                                        87: 'waves_1',
                                        99: 'c',
                                        67: 'C',
                                        101: 'e',
                                        102: 'f',
                                },
            }

stateLessKeys = {
                'single': ['c', 'C', 'f', 'e', 'p', 'hold'],
                'basics': ['c', 'C'],
                }


# some additional service funcs
def add_waitKeys(waits, states, r, *args, **kwargs):
        # waitKeys are recognized on press and actions can be initialized
    r.waitKeys.update(waitKeySets.get(waits))
    # stateLessKeys do not change stateKey when pressed 
    # they are only excecuted as long as key is pressed
    r.stateLessKeys.extend(stateLessKeys.get(states))
    if kwargs.get('verbose'):
        print('\navailable waitKeys: NOTE: press <space> to hold')
        for k, v in r.waitKeys.items(): 
            print(f"\t{v[0].upper() if v.endswith('1') else v[0]}  {k} -> {v}")