# contract.py
from itertools import count
from collections import defaultdict
import colorama as color
color.init()
from tabulate import tabulate
check = count(0)

def check_kwargs(verbose, **kwargs):
    errors = defaultdict(list)
    for k, vs in kwargs.items():
        try:
            if k == 'dims':
                if (len(vs) == 3) and (ret := vs[-1]) != 3:
                    num = next(check)
                    errors['kwargs'].append(f"{num} dims: dims[-1] must be 3 but is: {ret}")
                elif not (2 <= len(vs) <= 3):
                    num = next(check)
                    errors['kwargs'].append(f"{num} dims: must be 2 or 3 but is: {len(k)}")
            if k == 'numObjs':
                if kwargs['moduleName'].endswith('single') and vs != 1:
                    num = next(check)
                    msg = f"numObjs must be 1 for module {kwargs['moduleName']} but is {vs}"
                    errors['kwargs'].append(f"{num} {k}: {msg}")
        except Exception as e:
            print(f"e: \n{e}")
    print('\n')
    msg = tabulate(errors, headers='keys', tablefmt='psql', showindex=True)
    assert not errors, f"\n{color.Fore.RED}{msg}{color.Style.RESET_ALL}"
    return kwargs

def update_kwargs(verbose, **kwargs):
    if len(kwargs['dims']) == 2:
        kwargs['dims'].append(3)
    return kwargs