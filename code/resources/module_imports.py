import inspect, os, sys
import colorama as color
color.init()
from collections import defaultdict
import importlib
from contextlib import contextmanager


# replace at python 3.9
from typing import List, Dict

def find_objs(testModule:object,
                ignoreFuncs:list = [],
                classNames:list = [],
                funcNames:list = [],
                **kwargs:dict,
    ) -> dict:
    """ <br><br>
    *Last update: 2021-06-04*
    # takes a module and returns specified Classes, Functions from it
    # -> dict
    
    ########################### SHELL EXAMPLE ###########################
    ### SHELL
    # no shell call available
    
    ########################### CODE EXAMPLE ###########################
    ### INSTANTIATE
    import sys
    sys.path.append('/python_venvs/999_snippets/python_buildins')
    from module_imports import find_objs, import_module
    moduleFileName: str = '/python_venvs/testopia/testing/testmodules/package_a/testing1.py'
    
    ### INPUTS
    testModule: object = import_module(moduleFileName)
    ignoreFuncs: list = []
    classNames: list = []
    funcNames: list = ['add_three_lists']
    kwargs: dict = {'ignoreFuncs': ignoreFuncs, 'classNames': classNames, }

    ### RUN Funciton, use following statement
    funcOut: object = find_objs(testModule, funcNames = funcNames, **kwargs)
    print(f'module_import.find_objs, funcOut: {funcOut}')
    
    
    ########################### TEST ###########################
    ### ASSERTION
    # this tests function find_objs
    funcOutPrep: str = str(list(funcOut.values())[0][0])[:36]
    expected: str = '<function TestClass1.add_three_lists'
    ########################### END TEST ###########################
    
    """
    testObjs = defaultdict(list)
    for name, obj in inspect.getmembers(testModule):
        if not inspect.isclass(obj) and not inspect.isfunction(obj): continue
        if not obj.__module__.startswith(testModule.__name__): continue
        # resulting testObjs are of two kinds: 1 {Class: [funcs]} and/or 2 {module: [funcs]}
        if inspect.isclass(obj) and (name in classNames or not classNames):
            for funcName, testFunc in inspect.getmembers(obj):
                if inspect.isfunction(testFunc)\
                                and funcName not in ignoreFuncs\
                                and (not funcNames or funcName in funcNames):
                    testObjs[obj].append(testFunc)
        elif inspect.isfunction(obj)\
                            and name not in ignoreFuncs\
                            and (not funcNames or name in funcNames):
            testObjs[testModule].append(obj)
    msg = f'module_imports.find_objs, no testObjs found in: {testModule.__name__}'
    assert testObjs, f"{color.Fore.RED}{msg}{color.Style.RESET_ALL}"
    return testObjs

def get_objs(prcsData, modules):
    """#.+NOT ACTIVE.+#"""
    for prc, params in prcsData.items():
        params.update(get_obj(params, modules[prc]))
    return prcsData

def get_obj(params:Dict[str, str], module):
    """
        *Last update: 2021-06-04*
    """
    objs = {}
    for i, (name, obj) in enumerate(inspect.getmembers(module)):
        funcName = params['funcName']
        if inspect.isclass(obj)\
        and (obj.__module__.endswith(module.__name__)\
        and obj.__name__ == params['className']):
            for _, function in inspect.getmembers(obj):
                try:
                    if inspect.isfunction(function) and function.__name__ == funcName:
                        objs['_class'] = obj
                        objs['function'] = function
                        break
                except Exception as e:
                    print(f"funcName: {funcName} returned with {e}")
            if objs.get('function'): break
        elif inspect.isfunction(obj) and obj.__name__ == funcName:
            objs['_class'] = None
            objs['function'] = obj
            break
    msg = f"module_imports.get_obj: No class or function found! {module}"
    assert objs, f"{color.Fore.RED}{msg}{color.Style.RESET_ALL}"
    return objs

def import_module(*args, filePath:str, importPaths:list=[], **kwargs):
    """ <br><br>
    *Last update: 2021-06-04*
    # takes a filePath (complete path to module), and imports the module
    # returns a dictio
    
    ########################### SHELL EXAMPLE ###########################
    ### SHELL
    # no shell call available
    
    ########################### CODE EXAMPLE ###########################
    ### INSTANTIATE
    from module_imports import import_module
    
    ### INPUTS
    filePath: str = '/python_venvs/testopia/testing/testmodules/package_a/testing.py'
    
    ### RUN Function, use following statement
    funcOut: object = import_module(filePath)
    print(f'module_import.import_module, funcOut: {funcOut}')
    
    
    ########################### TEST ###########################
    ### ASSERTION
    # this tests module_imports.py
    funcOutPrep: str = str(funcOut)[:17]
    expected: str = "<module 'testing'"
    ########################### END TEST ###########################
    
    """
    sys.path.extend(importPaths)
    moduleDirPath, fileName = os.path.split(filePath)
    sys.path.append(moduleDirPath)
    sys.path = sorted(list(set(sys.path)))
    # experimental attempt to harmoize imports worked for cadwalks, however might fail
    # in other case because different sorting os sys.path might be required
    with tempdir(moduleDirPath, *args, **kwargs):
        imported = importlib.import_module(os.path.splitext(fileName)[0])
    return imported

def import_modules(modulesParams, **kwargs):
    """imports module and appends it to process dicitonary"""
    modules = {}
    for name, param in modulesParams.items():
        modules[name] = import_module(**param)
    return modules

def get_objs_from_file(moduleFileName, **kwargs):
    module = import_module(moduleFileName, **kwargs)
    testObjs = find_objs(module, **kwargs)
    return testObjs


@contextmanager
def tempdir(moduleDirPath, *args, **kwargs):
    CWD = os.getcwd()
    os.chdir(moduleDirPath)
    try:
        yield
    except Exception as e:
        print(f"\nmodule_imports.tempdir.ERROR: {e}\n")
    finally:
        os.chdir(CWD)