# numpy_collections.py
import numpy as np
from PIL import Image

def mk_wave(sLen:int, fq:float, amp:float=1., offset:float=0., shift:float=1.) -> np.ndarray:
    start = 2*np.pi * offset
    # full length signal ends after full cycle but must stop 1 step before
    signal = np.linspace(start, start + 2*np.pi * fq, sLen+1)[:-1]
    # wave is multiplied by amplitude and can be shifted i.e. to wave arround 1 instead of 0
    wave = np.cos(signal) * amp + shift
    # draw_line(wave, 'mk_wave') #NOTE: this will interrupt progam exec
    return wave

def normalize(vtx, *args, zeroCenter=True, downScale=True, **kwargs):
    if zeroCenter: vtx -= vtx.mean(0)
    if downScale: vtx /= vtx.max()
    return vtx

def load_image(imgPath, dims):
    # flip is needed to convert RGB to BGR values
    img = Image.open(imgPath)
    if dims:
        img = img.resize(dims[:2])
    imgArray = np.flip(np.array(img.convert('RGB'), dtype=np.int32), axis=-1)
    return imgArray

def draw_line(data, func):
    import matplotlib.pyplot as plt
    import matplotlib.style as style
    plt.title(func)
    plt.xlabel('total signal length') #usually input or lengh of array
    plt.ylabel('wave amplitude') #usually output of a function
    ax1 = plt.hlines(data.mean(), 0, len(data), colors='b', linestyle='dashed')
    plt.legend([ax1,], ['data_mean'])
    # this requires a 1-d array
    plt.plot(range(data.shape[0]), data, label='data-line', color='r')
    style.use('ggplot')
    plt.show()
    quit()