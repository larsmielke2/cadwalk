from numba import njit
import numpy as np

def rotate2d(pts:np.ndarray, rads:float, cplx:bool=False):
    rot = pts * (np.cos(rads) + 1j * np.sin(rads))
    return rot if cplx else np.stack((rot.real, rot.imag), axis=1)

@njit(nopython=True, nogil=True, cache=True, parallel=False)
def rotate3d(vtx:np.ndarray, cumRad:np.ndarray) -> np.ndarray:
    """
        rotates verteces of shape=(n:3) using cumRads of shape=(3,)
        NOTE: for list of verteces use rotate3ds funcion below
        call like: vtx = rotate3d(vtx, (1/4*np.pi, 0, 0))
    """
    rm = np.array([
                    [
                    [ np.cos(cumRad[0]),  -np.sin(cumRad[0]),  0. ], 
                    [ np.sin(cumRad[0]),   np.cos(cumRad[0]),  0. ],
                    [ 0.,                                 0.,  1. ],
                    ],

                    [
                    [  np.cos(cumRad[1]), 0.,  np.sin(cumRad[1])  ], 
                    [  0.,                1.,                 0.  ],
                    [ -np.sin(cumRad[1]), 0.,  np.cos(cumRad[1])  ],
                    ],

                    [
                    [ 1.,                 0.,                  0. ],
                    [ 0.,  np.cos(cumRad[2]),  -np.sin(cumRad[2]) ], 
                    [ 0.,  np.sin(cumRad[2]),   np.cos(cumRad[2]) ],
                    ],

                    ], dtype=np.float32)
    return vtx @ (rm[0] @ rm[1] @ rm[2])

def rotate3ds(vtxs:list, cumRads:list) -> list:
    """
        rotates list of objects or array of objects using rotate3d
        call like: vtx = rotate3ds([vtx, ...], [(1/4*np.pi, 0, 0), ...])
    """
    return [rotate3d(vtx, cumRad) for vtx, cumRad in zip(vtxs, cumRads)]

def rotate_3d_cum(vtx:np.ndarray, source:np.ndarray, cumRads:np.ndarray, offsets:np.ndarray,
    ) -> np.ndarray:
    """
        takes a vtx array, rotates it len(cumRads) times and records every intermediate state
        inputs: target object, source object, rads upper bound, point offset before rotation
    """
    # shift points and rotate
    source += offsets
    # rotation has to be repeated len(cumRads) times. Creates shape len(source) * len(cumRads)
    for i, dRad in enumerate(cumRads):
        vtx[len(source) * i: len(source) * (i + 1)] = rotate3d(source, dRad)



"""
    ########################### EXPERIMENTALS ###########################
"""
def experimental_twist( vtx:np.ndarray, dims:np.ndarray, numTwists:float, dim:int=1 ) -> np.ndarray:
    alpha = np.array([[0., 0., 0.], [2.*np.pi, 0.*np.pi, 0.*np.pi]], dtype=vtx.dtype)
    alpha *= numTwists
    cumRads = np.linspace(*alpha, dims[dim]).astype(vtx.dtype)
    return np.vstack([rotate3d(v - v.mean(0), cumRad) + v.mean(0) \
                        for v, cumRad in zip(vtx.reshape(*dims[::-1], 3), cumRads)])