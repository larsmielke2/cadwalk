# settings.py
import numpy as np
# from typing import Protocol
import os

ints = np.int32
floats = np.float32
uInts = 'uint8'

basePath = os.path.abspath(os.path.split(os.path.split(__file__)[0])[0])
mediaPath = os.path.join(os.path.split(basePath)[0], 'media')

imgPath = os.path.join(mediaPath, 'images')
stlsPath = os.path.join(mediaPath, 'stls')