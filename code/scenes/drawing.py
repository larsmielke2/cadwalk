# drawing.py
import cv2
from itertools import cycle, zip_longest
import numpy as np
# from numba import njit, prange
import time
import datetime as dt

import os, re, sys
import cadwalk.code.resources.settings as sts

np.set_printoptions(edgeitems=30, linewidth=100000, suppress=True)

class Scene:

    def __init__(self, *args,
                    name:str='main',
                    verteces:bool=True, points:bool=False, edges:bool=False, faces:bool=False,
                    sceneShape,
                    padding:int=10,
                    record:bool=False,
                    verbose:int=0,
                    **kwargs
                ):
        """
            takes a (n, 3) array and renders it into a canvas
            call lke: Scene(sceneShape=sceneShape, rotated=g.vtx.copy(),
                                color=color, mesh=mesh, edges=True, faces=Tru
        """
        # session elements
        self.name = name
        self.verbose = verbose
        # setup window elements
        self.sceneShape = np.array(sceneShape, dtype=sts.ints)
        self.center = self.sceneShape[:2] // 2
        self.bounds = self.mk_bounds(padding)
        self.canvas = np.zeros((*self.sceneShape[:2], 3), dtype=sts.uInts)
        self.bgrColor = 0
        self.msgColor = (255, 255, 255)
        # rotated and color as BGR ! lists containing arrays of shape (n, 3)
        self.rotateds, self.colors, self.meshes = [], [], []
        # boolean parameters for display
        self.verteces, self.points, self.edges, self.faces = verteces, points, edges, faces
        self.numPoints = 0

    def mk_bounds(self, padding, *args, **kwargs):
        return np.stack(
                        [np.zeros((3)) + padding, np.array(self.sceneShape) - padding], 
                        axis=0
                        ).astype(sts.ints)

    def draw_rotated_vtxs(self, rots, color, order, *args, **kwargs):
        self.canvas[tuple(rots[order].T[:2].astype(sts.ints))] = color[order]
       

    def draw_rotated_circles(self, rots, color, order, *args, **kwargs):
        for pt, c in zip(tuple(rots[order][:, :2][:, ::-1].astype(sts.ints)), color[order]):
            self.canvas = cv2.circle(self.canvas, pt.tolist(), radius=1, 
                                    color=c.tolist(), thickness=3)
    

    def draw_mesh(self, rots, color, mesh, order, *args, **kwargs):
        """
            draws a mesh grid for a set of given rotated
            NOTE:
                rotated.shape = (m, 3) for m verteces
                order.shape = (min(m, 0),) contains visibles ix in back to front order
                mesh.shape = (2m, 3) 2m triangles with each in 3 referring to 1 rotated ix
        """

        # there are two mesh sections resulting from concatenation in Geometry.mk_mesh
        # both sections are len(self.rotateds) apart and both have to be ordered identically
        od = np.stack([order, order + len(rots)], 1).flatten()
        for tr, color in zip(
                                np.flip(rots[mesh[od]][..., :2].astype(sts.ints), -1), 
                                color[mesh[od]][:, 0, :].tolist() ):
            # tr.shape == [3, 2]
            cv2.drawContours(self.canvas, [tr], 0, color, -1 if self.faces else 1)

    def sort_and_filter(self, rots, *args, **kwargs):
        visibles = np.argwhere(
                                    ~(rots[:, :2] <= self.bounds[0, :2]).any(-1) &
                                    ~(rots[:, :2] >= self.bounds[1, :2]).any(-1)
                                    ).astype(sts.ints).flatten()
        order = np.argsort(rots[:, 2])[::-1].astype(sts.ints)
        if len(order) != len(visibles):
            order = order[np.in1d(order, visibles)]
        return order

    def draw(self, rotateds, colors=None, meshes=[], *args, **kwargs):
        self.numPoints = len(rotateds) * len(rotateds[0])
        self.canvas[:] = self.bgrColor
        if colors is None: colors = np.full(rotateds.shape, 255, dtype=np.int32)
        # all objects should be rendered from back to front 
        # remove vtx which are outside of the screen
        if rotateds.ndim > 3:
            return self.draw( rotateds[0], colors[0], meshes[0], *args, **kwargs )
        for i, (rots, color, mesh) in enumerate(zip_longest(rotateds, colors, meshes)):
            # print(f"i, rots[:5]: \n{i, rots[:5]}")
            order = self.sort_and_filter(rots, *args, **kwargs)
            if self.points:
                self.draw_rotated_circles(rots, color, order, *args, **kwargs)
            elif self.verteces:
                self.draw_rotated_vtxs(rots, color, order, *args, **kwargs)
            if self.edges or self.faces:
                self.draw_mesh(rots, color, mesh, order, *args, **kwargs)