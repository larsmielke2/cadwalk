# single_object.py
from scenes.drawing import Scene


def single_object_scene(*args, **kwargs):
    kwargs['s'] = Scene(*args, edges=False, faces=False, **kwargs)
    return kwargs