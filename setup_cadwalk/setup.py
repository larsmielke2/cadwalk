from setuptools import setup

setup(
        name = 'cadwalk',
        version = '0.1',
        py_modules = ['cadwalk'],
        install_requires = ['colorama', 'PyYAML', 'tabulate'],
        entry_points = """ 
                        [console_scripts]
                        tto=cadwalk:main
                        """    
    )