import numpy as np

from cadwalk.code.scenes.drawing import Scene
from cadwalk.code.renderer.renderer import Render
from cadwalk.code.renderer.waitkeys import add_waitKeys
from cadwalk.code.resources.rotations import rotate3d
from cadwalk.code.renderer.orbit_controlls import basic_orbit_controlls
from cadwalk.code import arguments


args, kwargs = [], arguments.mk_args().__dict__
# sceneShape = (500, 500, 3)
line = np.linspace((-130, -140, -150), (130, 140, 150), 50, dtype=np.float32)
rotateds = line.copy()
colors = np.zeros(line.shape).astype(np.int32)
colors[..., 1] += 255
# canvas = np.zeros(sceneShape[:2], dtype="uint8")
# canvas[tuple(line[:, :2].T.astype(np.int32))] = 255


s = Scene(*args, **kwargs)
r = Render(*args, **kwargs)
add_waitKeys('basics', 'basics', r=r)
cumRads = np.array([0.1, 0.1, 0.1], dtype=np.float32)
dRads = np.array([5e-3, 5e-3, 5e-3], dtype=np.float32)


while r.running:
    rotateds[:] = basic_orbit_controlls(line, r, basicRot=dRads)
    s.draw(rotateds[None, ...], colors[None, ...])
    r.render(s.canvas)

